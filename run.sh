#!/usr/bin/env bash

# admin password defaults to secret if corresponding env var not set
: ${ADMIN_PASSWORD:=secret}

docker run --rm --detach --publish 8080:8080 -e "JAVA_OPTIONS=-DADMIN_PASSWORD=$ADMIN_PASSWORD" --name jetty-server code-elevator
