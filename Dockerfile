FROM maven:3.3.9-jdk-8-onbuild as install

FROM jetty:9.3.11-jre8 as run
COPY --from=install /usr/src/app/elevator-server/target/*.war /var/lib/jetty/webapps/root.war